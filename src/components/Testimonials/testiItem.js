export const testimonials = [
  {
    name: "John Dee 32, Bromo",
    img: "/images/img_1.png",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, ad quod. Quos aut tenetur adipisci quis harum fugiat, error accusamus quisquam.",
  },
  {
    name: "Johny 32,Bromo",
    img: "/images/img_2.png",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil blanditiis sapiente enim dolorum vero commodi est voluptatibus? Incidunt sit saepe dignissimos.",
  },
  {
    name: "Jhonson 40, Bandung",
    img: "/images/img_3.png",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum illo ea alias quae eveniet itaque, dolorum ducimus! Sequi dolores consequatur laborum placeat itaque!",
  },
];
